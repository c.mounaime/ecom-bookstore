import { ICustomerComment } from './../../shared/model/customer-comment.model';
import { CustomerCommentService } from './../../entities/customer-comment/customer-comment.service';
import { Component, Input, OnInit } from '@angular/core';
import { IBook } from 'app/shared/model/book.model';

/*
import { SearchWithPagination } from 'app/shared/util/request-util';
import { flatMap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { of, EMPTY } from 'rxjs';
*/

/**
 * @author QCR
 * Composant listant les commentaires des utilisateurs sur un livre.
 * Dans la premiere iteration, on va se contenter d'afficher seulement un commentaire.
 * TODO(1): Fournir un resumé du rating des clients ( note moyenne, nombre de client pas note)
 * TODO(2):Affichage des commentaires, ainsi que possibilité de charge la prochaine page de commentaire.
 */

@Component({
  selector: 'jhi-customer-review-list',
  templateUrl: './customer-review-list.component.html',
  styleUrls: ['./customer-review-list.component.scss'],
})
export class CustomerReviewListComponent implements OnInit {
  @Input() book: IBook | undefined;
  averageStar!: number;

  @Input() customerComments: ICustomerComment[] | undefined;
  constructor(private customerCommentService: CustomerCommentService) {}

  ngOnInit(): void {
    this.averageStar = -1;
    /*
    if(this.book){
      this.averageStar = this.book.rating!;
      const searchParams : SearchWithPagination = {
        query : "bookId:" + this.book.id!,
        page  : 0,
        size  : 15,
        sort  : []
      };


      
      * TODO: Search by bookId

      this.customerCommentService.search(searchParams).subscribe( (customerComments : HttpResponse<ICustomerComment[]>) =>{
        if(customerComments.body){
          this.customerComments = customerComments.body;
        }  
      }) 

    }*/

    // Temporaire
    this.customerCommentService.query().subscribe(res => {
      this.customerComments = res.body!;
    });
  }
}
