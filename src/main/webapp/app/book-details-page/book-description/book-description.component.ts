import { Component, Input, OnInit } from '@angular/core';
import { IBook } from '../../../app/shared/model/book.model';
import { HttpResponse } from '@angular/common/http';
import { IOrder } from 'app/shared/model/order.model';
import { IOrderLine, OrderLine } from 'app/shared/model/order-line.model';
import { OrderService } from 'app/entities/order/order.service';
import { OrderLineService } from 'app/entities/order-line/order-line.service';
import { ICategory } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category/category.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'jhi-book-description',
  templateUrl: './book-description.component.html',
  styleUrls: ['./book-description.component.scss'],
})
export class BookDescriptionComponent implements OnInit {
  @Input() book: IBook | null | undefined;
  categoryName: string | undefined;

  constructor(
    private orderService: OrderService,
    private orderLineService: OrderLineService,
    private categoryService: CategoryService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    // eslint-disable-next-line no-shadow
    this.activatedRoute.data.subscribe(({ book }) => {
      this.book = book;
      this.categoryService.find(book.id).subscribe((response: HttpResponse<ICategory>) => {
        if (response.body) {
          this.categoryName = response.body?.name;
        }
      });
    });
  }

  /**
   * @author Emez
   * Event triggered when the user clicks on the AddToCart button from description page
   *
   * adds an order line of a given quantity to the user's cart or updates the quantity of an existing order line in the user's cart
   * and updates the price of the shopping cart after either operation
   */
  addToCart(qty: string): void {
    const quantity = Number.parseInt(qty, 10);
    if (quantity <= 0) {
      alert('Enter a valid quantity');
      return;
    }

    this.orderService.findCart(3).subscribe(
      (res: HttpResponse<IOrder>) => {
        const myCart: IOrder | null = res.body;

        if (myCart != null) {
          // custom user has an Order IN_CART
          // create the OrderLine & initialise some attributes
          const orderLine: IOrderLine = new OrderLine();
          orderLine.orderId = myCart.id;
          orderLine.bookId = this.book!.id;
          orderLine.quantity = quantity;
          orderLine.price = orderLine.quantity * this.book!.price!;

          // get the order lines in myCart
          this.orderLineService.findAllByOrderId(myCart.id!).subscribe((lines: HttpResponse<IOrderLine[]>) => {
            const myCartOrderLines: IOrderLine[] = lines.body!;

            if (this.orderLineAlreadyInCart(orderLine, myCartOrderLines)) {
              // then update the OrderLine
              alert('Update book : ' + this.book!.title + ' quantity in cart');
              const existingOrderLine = this.getExistingOrderLine(orderLine, myCartOrderLines);
              if (existingOrderLine != null) {
                const existingOrderLinePrice = existingOrderLine.price!;
                orderLine.id = existingOrderLine.id;
                this.orderLineService.update(orderLine).subscribe((updatedOrderLine: HttpResponse<IOrderLine>) => {
                  const persistentLine: IOrderLine = updatedOrderLine.body!;

                  myCart.totalPrice! = (myCart.totalPrice! * 100 - existingOrderLinePrice * 100 + persistentLine.price! * 100) / 100;
                  this.orderService.update(myCart).subscribe(() => {});
                  alert('Updated item in cart : ' + persistentLine.id);
                });
              }
            } else {
              // then create the OrderLine
              alert('Add book : ' + this.book!.title + ' to cart');
              this.orderLineService.create(orderLine).subscribe((addedOrderLine: HttpResponse<IOrderLine>) => {
                const persistentLine: IOrderLine = addedOrderLine.body!;
                alert('Added to cart : ' + persistentLine.id);

                // update the price of the shopping cart
                // multiply by 100, and finally divide by 100 to handle float
                myCart.totalPrice! = (myCart.totalPrice! * 100 + persistentLine.price! * 100) / 100;
                this.orderService.update(myCart).subscribe(() => {});
              });
            }
          });
        }
      },
      () => {
        console.log('Error in add to cart subscribe');
      },
      () => {
        console.log('Add to cart complete'); /* this.updateTotalPriceOfOrder(3); */
      }
    );
  }

  /**
   * @author Emez
   * @param orderLine: the OrderLine to be added to cart
   * @param orderLines: the OrderLines already in the shopping cart
   * @return true if the book in orderLine is already in one of the orderLines
   * @return false otherwise
   */
  orderLineAlreadyInCart(orderLine: IOrderLine, orderLines: IOrderLine[]): boolean {
    for (let i = 0, len = orderLines.length; i < len; i++) {
      if (orderLine.bookId === orderLines[i].bookId) return true;
    }
    return false;
  }

  /**
   * @author Emez
   * @param orderLine: the OrderLine to be updated in the cart
   * @param orderLines: the OrderLines already in the shopping cart
   * @return the orderLine with the same book as the orderLine to be updated or null if not found
   */
  getExistingOrderLine(orderLine: IOrderLine, orderLines: IOrderLine[]): IOrderLine | null {
    for (let i = 0, len = orderLines.length; i < len; i++) {
      if (orderLine.bookId === orderLines[i].bookId) return orderLines[i];
    }
    return null;
  }
}
