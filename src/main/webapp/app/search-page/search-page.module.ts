import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EcomBookstoreIndexBodyModule } from 'app/index-body/index-body.module';
import { SearchPageComponent } from './search-page.component';
import { searchPageRoute } from './search-page.route';

import { EcomBookstoreSharedModule } from '../shared/shared.module';
import { BookCardComponent } from './book-card/book-card.component';

@NgModule({
  imports: [EcomBookstoreSharedModule, EcomBookstoreIndexBodyModule, RouterModule.forChild(searchPageRoute)],
  declarations: [SearchPageComponent, BookCardComponent],
  exports: [SearchPageComponent, BookCardComponent],
  bootstrap: [SearchPageComponent, BookCardComponent],
})
export class EcomBookStoreSearchPageModule {}
