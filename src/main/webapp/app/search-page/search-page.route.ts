import { SearchPageComponent } from './search-page.component';

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

import { BookService } from '../entities/book/book.service';
import { IBook } from '../shared/model/book.model';

@Injectable({ providedIn: 'root' })
export class SearchPageResolve implements Resolve<IBook[]> {
  constructor(private service: BookService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBook[]> | Observable<never> {
    const query: String | null = route.queryParamMap.get('query');
    if (query) {
      return this.service.simpleSearch(query).pipe(
        flatMap((book: HttpResponse<IBook[]>) => {
          if (book.body) {
            return of(book.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return EMPTY;
  }
}

export const searchPageRoute: Routes = [
  {
    path: 'search/books',
    component: SearchPageComponent,
    resolve: {
      book: SearchPageResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];
