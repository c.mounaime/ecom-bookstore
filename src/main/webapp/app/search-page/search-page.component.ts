import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IBook } from '../shared/model/book.model';
import { BookService } from '../entities/book/book.service';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'jhi-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss'],
})
export class SearchPageComponent implements OnInit {
  books: IBook[] | null | undefined;
  query: String | undefined;
  constructor(private bookService: BookService, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.query = params['query'];
      this.bookService.simpleSearch(this.query).subscribe((res: HttpResponse<IBook[]>) => {
        this.books = res.body;
      });
    });
  }
}
